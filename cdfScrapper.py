#!/usr/bin/python3
# encoding=utf-8

import datetime
import os

import requests
from bs4 import BeautifulSoup
from odf import teletype
from odf.opendocument import OpenDocumentText
from odf.text import P
from progress.bar import Bar


def get_fil_page(fil_name: str, fil_page: int) -> BeautifulSoup:
    """Fetch a page from the fil.

    Args:
        fil_name: Name of the fil to fetch.
        fil_page: Page of the fil to fetch.

    Returns:
        HTML parsed BeautifulSoup object.
    """
    response = requests.get(
        f'https://cahiersdufootball.net/forum/{fil_name}',
        params={'page': fil_page},
        timeout=10
    )
    response.encoding = 'utf-8'
    return BeautifulSoup(response.text, 'html.parser')


if __name__ == '__main__':
    # Parameters
    FIL_ID = 'lost-horizons-259'
    output_dir = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'output'
    )

    # Variables
    PAGE = 1

    # Create output dir if not present
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # First, get the number of pages to fetch
    soup = get_fil_page(FIL_ID, PAGE)
    pagination = soup.findAll('li', class_='last')
    if len(pagination) > 0:
        last_page = int(pagination[0].findAll('a')[-1].get_text())
        bar = Bar('Processing', max=last_page)
    else:
        raise Exception('Failed to get last page')
    # Get the fil title
    title = soup.findAll('section', id='messages')
    if len(title) > 0:
        fil_title = title[0].findAll('h1')[0].get_text().replace(' ', '_')
        file_name = os.path.join(
            output_dir,
            f"{fil_title}_{datetime.date.today().strftime('%Y_%m_%d')}.odt"
        )
        print(file_name)
    else:
        raise Exception('Failed to get title')

    # Open an ODT file
    textdoc = OpenDocumentText()

    while True:
        # Fetch the page
        soup = get_fil_page(FIL_ID, PAGE)

        # Extract all messages
        messages = soup.find('ul', class_='paginated').findAll('li')

        # Write all messages to output file
        for message in messages:
            # Get data
            author = message['data-author']
            date = message.find('span', class_='date').get_text()
            content = message.find('div', class_='texte').get_text()

            # Build and add paragraph to ODT file
            para = P()
            teletype.addTextToElement(para,
                                    author + '\n' +
                                    date + '\n\n' +
                                    content + '\n\n' +
                                    '+-' * 46 + '\n')
            textdoc.text.addElement(para)

        # Save the ODT file
        textdoc.save(file_name)

        # Check if last page is reached
        if PAGE < last_page:
            PAGE = PAGE + 1
            bar.next()
        else:
            bar.next()
            bar.finish()
            break
