#!/usr/bin/python3
# encoding=utf-8

import os
import requests
import urllib
import datetime
from bs4 import BeautifulSoup
from progress.bar import Bar
from odf import teletype
from odf.opendocument import OpenDocumentText
from odf.text import P

# Parameters
archive_url = 'https://web.archive.org/web/'
url_base = 'http://www.cahiersdufootball.net/forum_fil.php'
fil_id = 72
user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0'
output_dir = os.path.dirname(os.path.realpath(__file__)) + '/output'
first_page = 62
last_page = 500

# Create output dir if not present
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

file_name  = output_dir + '/'
file_name += 'CM'.replace(' ', '_') + '_'
file_name += datetime.date.today().strftime('%Y_%m_%d') + '.odt'
print(file_name)

bar = Bar('Processing', index=first_page, max=last_page)

# Open an ODT file
textdoc = OpenDocumentText()

# Get all pages
for page in range(first_page, last_page):
    params={'id_forum_fil': fil_id, 'page': page}
    url = f'{archive_url}{url_base}?{urllib.parse.urlencode(params)}'
    r = requests.get(url, headers={'user-agent': user_agent})
    r.encoding = 'utf-8'
    if r.status_code != 200:
        print(f'Page {page} not found!')
        continue

    soup = BeautifulSoup(r.text, 'html.parser')

    # Extract all messages
    messages = soup.findAll('div', class_='message')

    # Write all messages to output file
    for message in messages:
        # Get data
        author = message.findAll('h2')[0].get_text()
        date = message.findAll('h3')[0].get_text()
        content = message.findAll('p')[0].get_text()

        if author == 'Vieux légume' and len(content) >= 500:
            # Build and add paragraph to ODT file
            para = P()
            teletype.addTextToElement(para,
                                    author + '\n' +
                                    date + '\n\n' +
                                    content + '\n\n' +
                                    '+-' * 44 + '\n')
            textdoc.text.addElement(para)

    # Save the ODT file
    textdoc.save(file_name)

    # Check if last page is reached
    if page < last_page:
        bar.next()
    else:
        bar.next()
        bar.finish()
