# CDF scrapper

Scrapper to retrieve all messages of a given discussion on the famous [Cahiers du Foot](http://www.cahiersdufootball.net/) website.

## Installation
Install the Python modules with:
```
pip3 install -r requirements.txt
```

## Usage
* Change the id of the discussion set in the `fil_id` variable.
* Run `./cdfScrapper.py`.
* Wait for all the pages to be fetch.
* The output ODT file is available in the `output` directory.
